FROM php:7.0-apache

EXPOSE 80

RUN apt update && apt install -y vim htop iputils-ping libmcrypt-dev libxml2-dev libjpeg62-turbo-dev libpng-dev zlib1g-dev software-properties-common

RUN docker-php-ext-install xml pdo_mysql mysqli zip mbstring mcrypt

RUN mkdir /app

WORKDIR /app

COPY ./vhost.conf /etc/apache2/sites-available/000-default.conf

RUN a2enmod rewrite

RUN groupadd -r -g 1000 ahmed && useradd -r -u 1000 -g ahmed ahmed

RUN mkdir -p /home/ahmed