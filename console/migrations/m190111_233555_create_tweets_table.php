<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tweets`.
 */
class m190111_233555_create_tweets_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable("tweets", [
			"id" => $this->primaryKey(),
			"description" => $this->text()->notNull(),
			"link" => $this->string()->notNull(),
			"comment" => $this->string()->notNull(),
			"created_at" => $this->dateTime()->notNull(),
			"created_by" => $this->integer()->notNull(),
			"updated_at" => $this->dateTime(),
			"updated_by" => $this->integer(),
			"status" => $this->smallInteger()->notNull()->defaultValue(10)
		]);

		$this->addForeignKey(
			"fk_tweets_created_by",
			"tweets",
			"created_by",
			"users",
			"id",
			"CASCADE"
		);

		$this->addForeignKey(
			"fk_tweets_updated_by",
			"tweets",
			"updated_by",
			"users",
			"id",
			"CASCADE"
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable("tweets");
	}
}
