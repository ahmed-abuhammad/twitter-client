<?php
namespace common\models;

use Yii;
use yii\base\Model;

class TwitterSearch extends Model
{
	private $_apiUrl = "https://api.twitter.com/1.1/search/tweets.json";
	private $_consumerKey = "";
	private $_consumerSecret = "";
	private $_accessToken = null;
	private $_count = 50;

	public __construct($accessToken)
	{
		$this->_accessToken = $accessToken;
	}

	public function search($query)
	{
		$url = $this->buildUrl($query);
	}

	private function buildUrl($query)
	{
		$finalUrl = $this->_apiUrl;
		$finalUrl .= "?q=" . urlencode($query);
		$finalUrl .= "&count=" . $this->_count;

		$header = $this->getAuthorizationHeader();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		$tweets = curl_exec($ch);
		curl_close($ch);

		return json_decode($tweets);
	}

	private function getAuthorizationHeader()
	{
		$header = 'authorization: OAuth ';
		$header .= 'oauth_consumer_key="{$this->_consumerKey}", ';
		$header .= 'oauth_nonce="", ';
		$header .= 'oauth_signature="", ';
		$header .= 'oauth_signature_method="HMAC-SHA1", ';
		$header .= 'oauth_timestamp="", ';
		$header .= 'oauth_token="{$this->_accessToken}", ';
		$header .= 'oauth_version="1.0"';
	}
}