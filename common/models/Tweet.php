<?php

namespace common\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "tweets".
 *
 * @property int $id
 * @property string $description
 * @property string $link
 * @property string $comment
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $status
 *
 * @property Users $createdBy
 * @property Users $updatedBy
 */
class Tweet extends \yii\db\ActiveRecord
{
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return "tweets";
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[["description", "link", "comment", "created_at", "created_by"], "required"],
			// [["description", "link", "comment"], "safe"],
			[["description"], "string"],
			[["description"], "trim"],
			[["created_at", "updated_at"], "safe"],
			[["created_by", "updated_by", "status"], "integer"],
			[["link", "comment"], "string", "max" => 255],
			[["link", "comment"], "trim"],
			[["created_by"], "exist", "skipOnError" => true, "targetClass" => User::className(), "targetAttribute" => ["created_by" => "id"]],
			[["updated_by"], "exist", "skipOnError" => true, "targetClass" => User::className(), "targetAttribute" => ["updated_by" => "id"]],
		];
	}

	public function beforeValidate()
	{
		$this->created_by = Yii::$app->user->id;
		$this->created_at = new Expression("NOW()");
		return parent::beforeValidate();
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			"id" => "ID",
			"description" => "Description",
			"link" => "Link",
			"comment" => "Comment",
			"created_at" => "Created At",
			"created_by" => "Created By",
			"updated_at" => "Updated At",
			"updated_by" => "Updated By",
			"status" => "Status",
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCreatedBy()
	{
		return $this->hasOne(User::className(), ["id" => "created_by"]);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUpdatedBy()
	{
		return $this->hasOne(User::className(), ["id" => "updated_by"]);
	}
}