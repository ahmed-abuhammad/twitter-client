<?php

use yii\helpers\StringHelper;
use yii\web\View;
/* @var $this yii\web\View */

$this->title = 'Twitter Client';
?>
<div class="site-index">

	<div class="jumbotron">
		<h1>Search Through Twitter!</h1>

		<form method="GET" action="/tweets/search">
			<input class="form-control" type="text" name="query" required="required" value="<?= $query; ?>">
		</form>
	</div>

	<button id="btn-save-all-tweets" class="btn btn-info">Save All!</button>
	<div class="tweets">
		<?php if(empty($tweets)): ?>
			<p class="alert alert-red">No results! :(</p>
		<?php else: ?>
			<table class="table table-responsive table-striped table-bordered table-tweets">
				<thead>
					<th><input id="checkbox-all-tweets" class="form-control" type="checkbox"></th>
					<th>Description</th>
					<th>Link</th>
					<th>Comment</th>
					<th>Actions</th>
				</thead>

				<tbody>
					<?php
						$count = 1;
						foreach($tweets as $tweet):
					?>
						<tr id="tweet-<?= $count; ?>" class="tweet">
							<td class="tweet-checkbox"><input class="form-control" type="checkbox" data-tweet-count="<?= $count; ?>"></td>
							<td class="tweet-description"><?= $tweet->text; ?></td>
							<td class="tweet-link">
								<?php if ($tweet->entities->urls[0]->expanded_url): ?>
									<a target="_blank" href="<?= $tweet->entities->urls[0]->expanded_url; ?>"><?= $tweet->entities->urls[0]->expanded_url; ?></a>
								<?php else: ?>
									No Link Specified
								<?php endif; ?>
							</td>
							<td class="tweet-comment"><input type="text"></td>
							<td class="tweet-actions"><button class="btn btn-info btn-save-tweet" data-tweet-count="<?= $count; ?>">Save</button></td>
						</tr>
					<?php
						$count++;
						endforeach;
					?>
				</tbody>
			</table>
		<?php endif; ?>
	</div>

</div>

<?php
$js = <<<JS

	function request(url, method, parameters, callback) {
		url =  "http://localhost:8080/api/" + url;

		$.ajax({
			url: url,
			method: method,
			dataType: "json",
			data: parameters,
			complete: function(response) {
				callback.call(this, response);
			}
		});
	}


	// Check/Uncheck all checkboxes
	$(document).on("click", "#checkbox-all-tweets", function() {
		var checked = this.checked;
		$(".tweet-checkbox input").each(function(id, checkbox) {
			checkbox.checked = checked;
		});
	});

	$(document).on("click", ".btn-save-tweet", function() {
		var btnSaveTweet = $(this);
		btnSaveTweet.text("Saving...");
		btnSaveTweet.removeClass("btn-success").addClass("btn-info");
		btnSaveTweet.attr("disabled", "disabled");

		var tweetCount = $(this).attr("data-tweet-count");
		var tweetElem = $("#tweet-" + tweetCount);
		var tweetCommentElem = tweetElem.find(".tweet-comment");
		if (!tweetCommentElem.find("input").val()) {
			tweetCommentElem.find("input").css({"border-color": "red"}) //append('<p class="alert alert-danger">Please add comment!</p>');
			$(this).text("Save");
			return;
		}

		var tweetDescription = tweetElem.find(".tweet-description").text();
		var tweetLink = tweetElem.find(".tweet-link").text();
		var tweetComment = tweetCommentElem.find("input").val();

		var data = {
			description: tweetDescription,
			link: tweetLink,
			comment: tweetComment
		};

		request("tweets", "POST", data, function(response) {
			if (response.status == 201) {
				btnSaveTweet.text("Saved");
				btnSaveTweet.attr("disabled", "disabled");
				btnSaveTweet.removeClass("btn-info").addClass("btn-success");
			} else {
				btnSaveTweet.text("Error");
				btnSaveTweet.removeClass("btn-info").addClass("btn-danger");
				btnSaveTweet.removeAttr("disabled");
			}
		});
	});

	$(document).on("click", "#btn-save-all-tweets", function() {

		var allTweetsData = [];
		$(".tweet-checkbox input:checked").each(function(id, checkbox) {
			var tweetElem = $("#tweet-" + $(checkbox).attr("data-tweet-count"));

			if (tweetElem.find(".tweet-comment input").val()) {
				allTweetsData.push({
					description: tweetElem.find(".tweet-description").text(),
					link: tweetElem.find(".tweet-link").text(),
					comment: tweetElem.find(".tweet-comment input").val()
				});
			}
		});

		request("tweets/bulk-create", "POST", {tweets: allTweetsData}, function(response) {
			if (response.status == 201) {
				alert("Tweets are added!");
			} else {
				alert("An error occurred!");
			}
		});
	});

JS;

$this->registerJs($js, View::POS_END);
?>