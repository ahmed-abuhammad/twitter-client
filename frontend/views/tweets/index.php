<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Tweets";
?>
<div class="tweet-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?php if (Yii::$app->session->hasFlash("tweet_delete_success")): ?>
		<p class="alert alert-success"><?= Yii::$app->session->getFlash("tweet_delete_success"); ?></p>
	<?php endif; ?>

	<?= GridView::widget([
		"dataProvider" => $dataProvider,
		"options" => [
			"class" => "table-responsive",
		],
		"columns" => [
			["class" => "yii\grid\SerialColumn"],

			"id",
			"description:text",
			"link",
			"comment",
			"created_at",
			//"created_by",
			//"updated_at",
			//"updated_by",
			//"status",

			[
				"class" => "yii\grid\ActionColumn",
				"buttons" => [
					"view" => function() { return false; },
					"update" => function($url, $model) {
						$html = '<a href="/tweets/' . $model->id . '" class="btn btn-info">Update</a>';
						return $html;
					},
					"delete" => function($url, $model) {
						$html = '<form method="POST" action="/tweets/' . $model->id . '" class="inline-form">';
						$html .= '<input type="hidden" name="_method" value="DELETE">';
						$html .= '<input type="hidden" name="' . Yii::$app->request->csrfParam . '" value="' . Yii::$app->request->csrfToken . '">';
						$html .= '<input type="hidden" name="redirect_url" value="' . Yii::$app->request->url . '">';
						$html .= '<input type="submit" name="submit" value="Delete" class="btn btn-danger">';
						$html .= '</form>';
						return $html;
					}
				]
			],
		],
	]); ?>
</div>
