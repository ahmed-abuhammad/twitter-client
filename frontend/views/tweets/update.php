
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Tweet */

$this->title = "Update Tweet: " . $model->id;
?>
<div class="tweet-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?php if (Yii::$app->session->hasFlash("tweet_update_success")): ?>
		<p class="alert alert-success"><?= Yii::$app->session->getFlash("tweet_update_success"); ?></p>
	<?php endif; ?>

	<div class="tweet-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, "comment")->textInput(["maxlength" => true]) ?>

	<div class="form-group">
		<?= Html::submitButton("Save", ["class" => "btn btn-success"]) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>

</div>