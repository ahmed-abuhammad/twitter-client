<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = "Settings";
?>
<div class="">
	<?php if (Yii::$app->session->hasFlash("settings_update_success")): ?>
		<p class="alert alert-success">
			<?= Yii::$app->session->getFlash("settings_update_success"); ?>
		</p>
	<?php endif; ?>
	<h1><?= Html::encode($this->title) ?></h1>

	<p>Use the following form to update your settings:</p>

	<div class="row">
		<div class="col-lg-5">
			<?php $form = ActiveForm::begin(["id" => "settings-form"]); ?>

				<?= $form->field($model, "first_name")->textInput(["autofocus" => true]) ?>

				<?= $form->field($model, "last_name") ?>

				<?= $form->field($model, "password")->passwordInput() ?>

				<?= $form->field($model, "password_retype")->passwordInput() ?>

				<div class="form-group">
					<?= Html::submitButton("Update", ["class" => "btn btn-primary", "name" => "settings-button"]) ?>
				</div>

			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
