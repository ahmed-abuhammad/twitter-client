<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\UserUpdateForm;

class UsersController extends Controller
{
	public function actionSettings()
	{
		if (Yii::$app->user->isGuest)
		{
			return $this->goHome();
		}

		$model = new UserUpdateForm();
		$user = Yii::$app->user->identity;

		if ($model->load(Yii::$app->request->post()) && $model->update($user))
		{
			$model->password = "";
			$model->password_retype = "";

			Yii::$app->session->setFlash("settings_update_success", "Settings updated successfully.");
		}
		else
		{
			$model->first_name = $user->first_name;
			$model->last_name = $user->last_name;
		}

		return $this->render("settings", [
			"model" => $model,
		]);
	}
}