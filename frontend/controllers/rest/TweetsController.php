<?php
namespace frontend\controllers\rest;

use Yii;
use yii\web\BadRequestHttpException;
use yii\rest\ActiveController;
use yii\filters\AccessControl;
use common\models\Tweet;

// The tweets rest controller
class TweetsController extends ActiveController
{
	public $modelClass = "common\models\Tweet";

	public function beforeAction($action)
	{
		Yii::$app->response->format = "json";
		return parent::beforeAction($action);
	}

	public function behaviors()
	{
		return [
			"access" => [
				"class" => AccessControl::className(),
				"rules" => [
					[
						"allow" => true,
						"roles" => ["@"]
					]
				]
			]
		];
	}

	// This method is for adding multiple tweets at once
	public function actionBulkCreate()
	{
		$tweetsData = Yii::$app->request->post("tweets");

		if (!is_array($tweetsData))
		{
			throw new BadRequestHttpException("Invalid Tweets Data.");
		}

		$success = true;
		$tweets = [];
		foreach ($tweetsData as $tweetData)
		{
			$tweet = new Tweet();
			$tweet->load($tweetData, "");
			$success = $success && $tweet->save();
			if ($success)
			{
				$tweets[] = $tweet;
			}
		}

		Yii::$app->response->format = "json";
		if ($success)
		{
			Yii::$app->response->statusCode = 201;
			return ["message" => "All tweets created.", "tweets" => $tweets];
		}

		Yii::$app->response->statusCode = 500;
		return ["message" => "An error occurred while saving the tweets."];
	}
}