<?php
namespace frontend\controllers;

use Yii;
use common\models\Tweet;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\base\ErrorException;
use Abraham\TwitterOAuth\TwitterOAuth;

class TweetsController extends Controller
{
	// List all the saved tweets
	public function actionIndex()
	{
		$query = Tweet::find();

		$dataProvider = new ActiveDataProvider([
			"query" => $query,
			"pagination" => [
				"pageSize" => 10
			]
		]);

		return $this->render("index", compact("dataProvider"));
	}

	// Update a saved tweet
	public function actionUpdate($id)
	{
		$model = Tweet::findOne($id);

		if (empty($model))
		{
			throw new NotFoundHttpException("The tweet you are looking for is not found!");
		}

		if ($model->load(Yii::$app->request->post()) && $model->save())
		{
			Yii::$app->session->setFlash("tweet_update_success", "Tweet updated successfully!");
			return $this->refresh();
		}

		return $this->render("update", compact("model"));
	}

	// Delete a saved tweet
	public function actionDelete($id)
	{
		$tweet = Tweet::findOne($id);

		if (empty($tweet))
		{
			throw new NotFoundHttpException("Tweet to delete not found!");
		}

		if ($tweet->delete())
		{
			Yii::$app->session->setFlash("tweet_delete_success", "Tweet deleted successfully!");
			$redirectUrl = Yii::$app->request->post("redirect_url");

			if (empty($redirectUrl))
			{
				$redirectUrl = "/tweets";
			}
			return $this->redirect($redirectUrl);
		}

		throw new ErrorException("An error occurred while deleting the tweet!");
	}

	// Search through twitter API
	public function actionSearch($query)
	{
		// The user must be logged in
		if (Yii::$app->user->isGuest)
		{
			return $this->goBack();
		}

		// The consumer ket and the consumer secret are stored in the application params
		$consumerKey = Yii::$app->params["twitter_consumer_key"];
		$consumerSecret = Yii::$app->params["twitter_consumer_secret"];

		// The oauth token and the oauth token secret will be stored in the session
		// upon successful authorization with twitter
		$userOauthToken = Yii::$app->session->get("user_oauth_token");
		$userOauthTokenSecret = Yii::$app->session->get("user_oauth_token_secret");

		// If the token is empty, then redirect to authorize with twitter.
		if (empty($userOauthToken) || empty($userOauthTokenSecret))
		{
			// Save the query upon page redirections
			Yii::$app->session->set("query", $query);
			return $this->redirect("/tweets/twitter-login");
		}

		// Use the abraham/twitteroauth to authorize with twitter and contact twitter API
		$twitter = new TwitterOAuth(
			$consumerKey,
			$consumerSecret,
			$userOauthToken,
			$userOauthTokenSecret
		);

		// Perform the search with the specified query
		$tweets = $twitter->get("search/tweets", ["q" => $query, "count" => 50, "lang" => "en"])->statuses;

		return $this->render("search", compact("tweets", "query"));
	}

	// Authorize with twitter
	public function actionTwitterLogin()
	{
		$twitterOAuth = new TwitterOAuth(Yii::$app->params["twitter_consumer_key"], Yii::$app->params["twitter_consumer_secret"]);

		// Request application token (to use it to request user oauth token)
		$request_token = $twitterOAuth->oauth(
			"oauth/request_token", [
				// Specify the oauth callback that the user wil be redirected to after authorization
				"oauth_callback" => "http://localhost:8080/tweets/twitter-oauth2-callback"
			]
		);

		if($twitterOAuth->getLastHttpCode() != 200)
		{
			throw new \Exception("There was a problem performing this request");
		}

		// Save the application token in the session
		Yii::$app->session->set("application_oauth_token", $request_token["oauth_token"]);
		Yii::$app->session->set("application_oauth_token_secret", $request_token["oauth_token_secret"]);

		// Generate the URL to make request to authorize our application
		$url = $twitterOAuth->url(
			"oauth/authorize", [
				"oauth_token" => $request_token["oauth_token"]
			]
		);

		return $this->redirect($url);
	}

	// The callback after authorization
	public function actionTwitterOauth2Callback()
	{
		// The OAuth verifier that returned from the authorization step
		$oauthVerifier = Yii::$app->request->get("oauth_verifier");
		$applicationOauthToken = Yii::$app->session->get("application_oauth_token");
		$applicationOauthTokenSecret = Yii::$app->session->get("application_oauth_token_secret");

		if (empty($oauthVerifier) || empty($applicationOauthToken) || empty($applicationOauthTokenSecret))
		{
			return $this->redirect("/tweets/twitter-login");
		}

		$twitterOAuth = new TwitterOAuth(
			$consumerKey,
			$consumerSecret,
			$applicationOauthToken,
			$applicationOauthTokenSecret
		);

		// Use the oauth verifier to request the user oauth token
		$token = $twitterOAuth->oauth(
			"oauth/access_token", [
				"oauth_verifier" => $oauthVerifier
			]
		);

		if (!$token)
		{
			// retry to login again if failed
			return $this->redirect("/tweets/twitter-login");
		}

		// Store the user oauth token and token secret in the session
		Yii::$app->session->set("user_oauth_token", $token["oauth_token"]);
		Yii::$app->session->set("user_oauth_token_secret", $token["oauth_token_secret"]);

		// Redirect to the search page on success
		$query = Yii::$app->session->get("query");
		return $this->redirect("/tweets/search?query=" . $query);
	}
}