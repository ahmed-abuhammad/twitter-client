<?php
namespace frontend\models;

use yii\base\Model;
use yii\db\Expression;

/**
 * User update form
 */
class UserUpdateForm extends Model
{
	public $first_name;
	public $last_name;
	public $password;
	public $password_retype;

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[["first_name", "last_name"], "trim"],
			[["first_name", "last_name"], "required"],
			[["first_name", "last_name"], "string", "max" => 255],
			[["first_name", "last_name"], "safe"],

			["password", "string", "min" => 6],
			["password", "checkPassword"],
			[["password", "password_retype"], "safe"]
		];
	}

	public function checkPassword($attributeName)
	{
		return $this->{$attributeName} == $this->password_retype;
	}

	public function update($user)
	{
		if (!$this->validate())
		{
			return null;
		}

		$user->first_name = $this->first_name;
		$user->last_name = $this->last_name;

		if ($this->password)
		{
			$user->setPassword($this->password);
		}

		$user->updated_at = new Expression("NOW()");

		return $user->save() ? $user : null;
	}
}
