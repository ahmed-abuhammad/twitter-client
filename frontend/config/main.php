<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'twitter-client',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'name' => 'Twitter Client',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-twitter-client',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-twitter-client', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'twitter-client',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'login' => 'site/login',
                'signup' => 'site/signup',
                'GET tweets/<id:\d+>' => 'tweets/update',
                'POST tweets/<id:\d+>' => 'tweets/update',
                'DELETE tweets/<id:\d+>' => 'tweets/delete',
                'GET api/tweets' => 'rest/tweets/index',
                'POST api/tweets' => 'rest/tweets/create',
                'POST api/tweets/bulk-create' => 'rest/tweets/bulk-create'
            ],
        ],

    ],
    'params' => $params,
];
